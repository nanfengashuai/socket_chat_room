# 韩顺平Java Socket聊天室

#### 介绍
（个人学习用）一个用Java多线程，集合，Socket编程，面向对象等内容编写的简易聊天室工程，后续会进行一些内容的补充、改进和完善。

#### 软件架构
软件架构说明
QQClient    ---    客户端程序
    qqclient    ---    客户端主要程序
        service    ---    客户端与服务器端实现通信的类
            ClientConnectServerThread.java    ---    客户端与服务器端通信的线程（线程持有一个Socket对象，保持两者之间的通信）
            FileClientServer.java    ---    完成客户端与服务器端之间的文件传输的类
            ManageClientConnectServerThread.java    ---    管理持有Socket对象的线程的类（主要通过集合进行线程的管理）
            MessageClientService.java    ---    完成客户端与服务器端之间的文字类消息的通信的类
            UserClientService.java    ---    完成用户登录验证和用户注册等功能的类
        utils    ---    用到的工具类
        view    ---    客户端界面程序
    qqcommon    ---    客户端关于消息的程序
QQServer    ---    服务器端程序


#### 安装教程

1.  直接在IEDA、eclipse等编译器中导入QQClient（客户端）和QQServer（服务器端）两个项目，运行即可。

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
